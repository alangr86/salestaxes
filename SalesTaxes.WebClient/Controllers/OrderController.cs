﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SalesTaxes.WebClient.Models;
using SalesTaxes.WebClient.Services.Interfaces;
namespace SalesTaxes.WebClient.Controllers
{
    public class OrderController : Controller
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IOrderService _orderService;

        public OrderController(ILogger<OrderController> logger, IOrderService orderService)
        {
            _logger = logger;
            _orderService = orderService;
        }

        public IActionResult Index()
        {
            Order order = _orderService.GetOrder();
            if (order != null)
              return  View(order);
            else
              return  RedirectToAction(nameof(Index), "Product");
        }

        public IActionResult CleanOrder()
        {
            _orderService.CleanOrder();
            return RedirectToAction(nameof(Index),"Product");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
