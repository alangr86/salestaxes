﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SalesTaxes.WebClient.Models;
using SalesTaxes.WebClient.Services.Interfaces;

namespace SalesTaxes.WebClient.Controllers
{
    public class ProductController : Controller
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IProductService  _productService;

        public ProductController(ILogger<OrderController> logger, IProductService productService)
        {
            _logger = logger;
            _productService = productService;

        }

        public IActionResult Index()
        {
            return View(_productService.GetProductsList());
        }

        public IActionResult Add(int id)
        {
            _productService.AddProduct(id);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Remove(int id)
        {
            _productService.Remove(id);
            return RedirectToAction(nameof(Index));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
