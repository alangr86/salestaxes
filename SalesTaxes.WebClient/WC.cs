﻿using System;
using System.Collections.Generic;
using SalesTaxes.WebClient.Models;

namespace SalesTaxes.WebClient
{
    /// <summary>
    /// cointain all the values static which the app need
    /// </summary>
    public static class WC
    {
        /// <summary>
        /// Key string of the Product list on the session
        /// </summary>
        public static string SessionProductList = "ProductListSession";

        /// <summary>
        /// Gets an initial list of products 
        /// </summary>
        /// <returns>List of Products</returns>
        public static List<Product> GetProductList()
        {
            List<Product>  productList = new List<Product>();
            productList.Add(new Product(1, "Book", 12.49, Enums.ProductType.Book, false, 0));
            productList.Add(new Product(2, "Music CD", 14.99, Enums.ProductType.Other, false, 0));
            productList.Add(new Product(3, "Chocolate bar", 0.85, Enums.ProductType.Food, false, 0));
            productList.Add(new Product(4, "Imported box of chocolates", 10.00, Enums.ProductType.Food, true, 0));
            productList.Add(new Product(5, "Imported bottle of perfume", 47.50, Enums.ProductType.Other, true, 0));
            productList.Add(new Product(6, "Imported bottle of perfume 2", 27.99, Enums.ProductType.Other, true, 0));
            productList.Add(new Product(7, "Bottle of perfume", 18.99, Enums.ProductType.Other, false, 0));
            productList.Add(new Product(8, "Packet of headache pills", 9.75, Enums.ProductType.Medicine, false, 0));
            productList.Add(new Product(9, "Imported box of chocolates 2", 11.25, Enums.ProductType.Food, true, 0));
            return productList;
        }
    }
}
