﻿using System;
using System.Text.Json;
using Microsoft.AspNetCore.Http;

namespace SalesTaxes.WebClient.Utility
{
    /// <summary>
    /// Class extending from HttpContext which allow to store a object or list of object on the session.
    /// </summary>
    public static class SessionExtention
    {
        /// <summary>
        /// Set a value to the object session 
        /// </summary>
        /// <typeparam name="T">Generic type parameter</typeparam>
        /// <param name="key">key string of the session value</param>
        /// <param name="value">Value to assing</param>
        public static void Set<T> (this ISession session, string key, T value)
        {
            session.SetString(key, JsonSerializer.Serialize(value));
        }


        /// <summary>
        /// Get the value of session data
        /// </summary>
        /// <typeparam name="T">Generic type parameter</typeparam>
        /// <param name="key">key string of the session value</param>
        /// <returns>object deserialize</returns>
        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default :  JsonSerializer.Deserialize<T>(value);
        }
    }
}
