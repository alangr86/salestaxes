﻿using System.Collections.Generic;
using System.Linq;
using SalesTaxes.WebClient.Models;
using SalesTaxes.WebClient.Utility;
using Microsoft.AspNetCore.Http;
using SalesTaxes.WebClient.Services.Interfaces;

namespace SalesTaxes.WebClient.Services.Implementation
{
    public class ProductService : IProductService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ProductService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public bool AddProduct(int idProduct)
        {
            List<Product> productList = GetProductsList();
            productList.ForEach(product => {
                if (product.ProductId == idProduct)
                {
                    product.Quantity = product.Quantity + 1;
                }

            });

            _httpContextAccessor.HttpContext.Session.Set(WC.SessionProductList, productList);

            return true;
        }

        public bool Remove(int idProduct)
        {
            List<Product> productList = GetProductsList();
            productList.ForEach(product => {
                if (product.ProductId == idProduct && product.Quantity > 0)
                {
                    product.Quantity = product.Quantity - 1;
                }
            });
            _httpContextAccessor.HttpContext.Session.Set(WC.SessionProductList, productList);
            return true;
        }

        public List<Product> GetProductsList()
        {
            List<Product> productList = new List<Product>();
            if (_httpContextAccessor.HttpContext.Session.Get<IEnumerable<Product>>(WC.SessionProductList) != null
                && _httpContextAccessor.HttpContext.Session.Get<IEnumerable<Product>>(WC.SessionProductList).Count() > 0)
            {
                productList = _httpContextAccessor.HttpContext.Session.Get<IEnumerable<Product>>(WC.SessionProductList).ToList();
            }
            else
            {
                productList = WC.GetProductList();
                _httpContextAccessor.HttpContext.Session.Set(WC.SessionProductList, productList);
            }
            return productList;
        }

    }
}
