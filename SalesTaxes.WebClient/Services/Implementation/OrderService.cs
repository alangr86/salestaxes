﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using SalesTaxes.WebClient.Models;
using SalesTaxes.WebClient.Services.Interfaces;
using SalesTaxes.WebClient.Utility;
using static SalesTaxes.WebClient.Models.Enums;

namespace SalesTaxes.WebClient.Services.Implementation
{
    public class OrderService: IOrderService
    {
        private readonly IProductService _productService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public OrderService(IHttpContextAccessor httpContextAccessor,IProductService productService)
        {
            _httpContextAccessor = httpContextAccessor;
            _productService = productService;
        }

        public Order GetOrder()
        {
            bool thereAreOrders = false;
            List<Product> productList = _productService.GetProductsList();
            List<OrderDetail> orderDetail = new List<OrderDetail>();
            Order order = new Order();
            order.Total = 0;
            order.TotalTaxes = 0;
            productList.ForEach(product =>
            {
                if (product.Quantity > 0)
                {
                    OrderDetail orderDetail = CreateOrderDetail(product);
                    order.ItemsOrderDetail.Add(orderDetail);
                    order.Total = order.Total + orderDetail.Total;
                    order.TotalTaxes = order.TotalTaxes + orderDetail.Taxes;
                    thereAreOrders = true;
                }
            });

            return thereAreOrders? order : null;
        }

        public void CleanOrder()
        {
            _httpContextAccessor.HttpContext.Session.Set(WC.SessionProductList, WC.GetProductList());
        }


        /// <summary>
        /// Create a new OrderDetail object based on the product
        /// </summary>
        /// <param name="product">Object of product type</param>
        /// <returns>Object of product type</returns>
        private OrderDetail CreateOrderDetail(Product product)
        {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.ProductId = product.ProductId;
            orderDetail.ProductName = product.Name;
            orderDetail.UnitPrice = product.Price;
            orderDetail.Quantity = product.Quantity;
            orderDetail.Taxes = 0;

            if (product.ProductType == ProductType.Other)
                orderDetail.Taxes = GetTaxes(product.Price, 10) * product.Quantity;
            if (product.IsImport)
                orderDetail.Taxes = orderDetail.Taxes + (GetTaxes(product.Price, 5));

            orderDetail.Taxes = RoundUp(Math.Ceiling(RoundUp(orderDetail.Taxes, 2) * 20) / 20, 2) * product.Quantity;

            orderDetail.Total = Math.Round(GetTotalPriceByProduct(product.Price, orderDetail.Taxes, product.Quantity), 2);

            return orderDetail;
        }

        /// <summary>
        /// Rounds a decimal value up
        /// </summary>
        /// <param name="input">Value to round</param>
        /// <param name="places">Number of zeros after point</param>
        /// <returns></returns>
        private double RoundUp(double input, int places)
        {
            double multiplier = Math.Pow(10, Convert.ToDouble(places));
            return Math.Ceiling(input * multiplier) / multiplier;
        }

        /// <summary>
        /// Calculate the total price per product
        /// </summary>
        /// <param name="price">Price of the product</param>
        /// <param name="totalTaxes">Taxess total of the product</param>
        /// <param name="quantity">number of product units</param>
        /// <returns>total price of the product</returns>
        private double GetTotalPriceByProduct(double price, double totalTaxes, int quantity)
        {
            var totalPrice = (price * quantity) + totalTaxes;

            return totalPrice;
        }

        /// <summary>
        /// Calculate the taxes price per product
        /// </summary>
        /// <param name="value">Price of the product</param>
        /// <param name="paxPercentage">Percentage value</param>
        /// <returns></returns>
        private double GetTaxes(double value, int paxPercentage)
        {
            double percentage;
            percentage = (value * paxPercentage) / 100;
            return percentage;
        }

    }
}
