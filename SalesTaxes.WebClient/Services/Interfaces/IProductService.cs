﻿using System.Collections.Generic;
using SalesTaxes.WebClient.Models;

namespace SalesTaxes.WebClient.Services.Interfaces
{
    public interface IProductService
    {
        /// <summary>
        /// Add a new product on the Shopping Cart
        /// </summary>
        /// <param name="idProduct">Id of the Product</param>
        /// <returns>bool value</returns>
        bool AddProduct(int idProduct);

        /// <summary>
        /// Delete a product of the Shopping Cart
        /// </summary>
        /// <param name="idProduct">Id of the Product</param>
        /// <returns>bool value</returns>
        bool Remove(int idProduct);

        /// <summary>
        /// Get the list of the products in the Shopping Cart
        /// </summary>
        /// <returns>List of Product object</returns>
        List<Product> GetProductsList();
    }
}
