﻿using SalesTaxes.WebClient.Models;

namespace SalesTaxes.WebClient.Services.Interfaces
{
    public interface IOrderService
    {
        /// <summary>
        /// Return the Order obj which contain the detail of the ticket
        /// </summary>
        /// <returns>Object of Order type</returns>
        Order GetOrder();

        /// <summary>
        /// Cleant the Order
        /// </summary>
        void CleanOrder();
    }
}
