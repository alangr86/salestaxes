﻿using System;
namespace SalesTaxes.WebClient.Models
{
    public class Enums
    {

        /// <summary>
        /// Define the product category 
        /// </summary>
        public enum ProductType
        {
            Food,
            Book,
            Medicine,
            Other
        }
    }
}
