﻿using System;
using System.Collections.Generic;
using static SalesTaxes.WebClient.Models.Enums;

namespace SalesTaxes.WebClient.Models
{
    public class Order
    {
        public List<OrderDetail> ItemsOrderDetail { get; set; } = new List<OrderDetail>();
        public double TotalTaxes { get; set; }
        public double Total { get; set; }
    }
}
