﻿using System;
using System.ComponentModel.DataAnnotations;
using static SalesTaxes.WebClient.Models.Enums;

namespace SalesTaxes.WebClient.Models
{
    public class Product
    {
        public Product()
        {

        }
        public Product(int id, string name, double price, ProductType productType, bool isImport,int quantity)
        {
            ProductId = id;
            Name = name;
            Price = price;
            ProductType = productType;
            IsImport = isImport;
            Quantity = quantity;
        }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public ProductType ProductType { get; set; }
        public bool IsImport { get; set; }

    }
}
