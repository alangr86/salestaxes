﻿using System;
namespace SalesTaxes.WebClient.Models
{
    public class OrderDetail
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double Taxes { get; set; }
        public double Total { get; set; }
    }
}
